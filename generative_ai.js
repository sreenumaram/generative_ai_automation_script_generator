
  // let promptEditor = document.querySelector("#prompt-editor");

  // ace.edit(promptEditor,{
  //     theme: 'ace/theme/dawn',
  //     mode: 'ace/mode/text',
  //     wrap: true,
  //     fontSize: '16px',
  //     showGutter: false

  // })

let outputEditor = document.querySelector("#editor");

ace.edit(outputEditor,{
    theme: 'ace/theme/clouds',
    mode: 'ace/mode/java',
    wrap: true,
    fontSize: '16px',
    showGutter: false

})

let editor = ace.edit(outputEditor);
// Disable text entry from UI
editor.setReadOnly(true);


// const axios = require('axios');
let objRepo;
async function getScript() {

  const fileInput = document.getElementById('fileInput');
  const file = fileInput.files[0];

  if (!file) {
    console.log('No file selected.');
    objRepo=" ";
    }
  // else if (!file.name .endsWith('.properties')) {
  //     console.error('Please select a .properties file.');
     
  //     }
      else{
        try {
    const fileContents = await readFile(file);
    objRepo = processData(fileContents);
    console.log("Processed data:", objRepo);
  } catch (error) {
    console.error("Error reading file:", error);
  }
      }


  // try {
  //   const fileContents = await readFile(file);
  //   x = processData(fileContents);
  //   console.log("Processed data:", x);
  // } catch (error) {
  //   console.error("Error reading file:", error);
  // }
   

  
  var templateDetails = await  document.getElementById("inputTemplate").value;
  var userPrompt = await  (  document.getElementById("inputText").value +"\n"+ await objRepo);
  // var userPrompt = await  (  document.getElementById("inputText").value +"\n"+ await objRepo)
  console.log(userPrompt);
 
  const OPENAI_API_KEY = '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';
  const headers = {
  'Content-Type': 'application/json',
  'Authorization': `Bearer ${OPENAI_API_KEY}`
  };


  const generateButton = document.getElementById("generateButton");
  // change button text to "Generating script"

  generateButton.textContent = "Generating script...";
  generateButton.disabled = true;
  const data = {
  "model": "gpt-3.5-turbo",
  "messages": [
  {
  "role": "system",
  "content": templateDetails 
 },
  {
  "role": "user",
  "content":userPrompt 
  }
  ],
  "temperature": 0.1
  };
  try {
  const response = await axios.post('https://api.openai.com/v1/chat/completions', data, { headers });
  console.log(response.data.choices[0].message.content);
  // document.getElementById("outputText").value =response.data.choices[0].message.content 
  ace.edit("editor").session.setValue(response.data.choices[0].message.content );
   // change button text to "Generate script"
//    ace.edit("editor").on("input", function() {
//     var lines = editor.session.getLength();
//     editor.setOption("showGutter", lines > 0); // Show line numbers if there are lines of text
// });

   generateButton.textContent = "Generate script...";
   generateButton.disabled = false;
   console.log(templateDetails);
   console.log(userPrompt)
  return response.data;
  } catch (error) {
  console.error('Error:', error.response ? error.response.data : error.message);
  throw error;
  }
  }
  



//   function copyOutput() {
//     var outputText = document.getElementById("prompt-editor");
//     outputText.select();
//     document.execCommand("copy");
    
//     // Create a message popup
//     var copyButton = document.querySelector(".copy-icon");
//     var messagePopup = document.createElement("span");
//     messagePopup.classList.add("message-popup");
//     messagePopup.textContent = "Copied!";
    
//     // Append the message popup next to the copy button
//     copyButton.parentNode.insertBefore(messagePopup, copyButton.nextSibling);
    
//     // Remove the message popup after 1.5 seconds
//     setTimeout(function() {
//         messagePopup.remove();
//     }, 1500);
// }



function copyText() {
  // Select the text inside the div
  var divText = document.getElementById("editor").innerText;

  
  // Create a temporary textarea element to hold the text
  var tempTextarea = document.createElement("textarea");
  tempTextarea.value = divText;
  document.body.appendChild(tempTextarea);
  
  // Select the text inside the textarea
  tempTextarea.select();
  
  // Copy the selected text to the clipboard
  document.execCommand("copy");
  
  // Remove the temporary textarea
  document.body.removeChild(tempTextarea);
  var copyButton = document.querySelector(".copy-icon");
  copyButton.textContent = "✓Copied!";
  // Reset the text after a certain delay (e.g., 2 seconds)
  setTimeout(function() {
      copyButton.textContent = "📋Copy code";
  },1000);


}


    function readFile(file) {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
          resolve(reader.result);
        };
        reader.onerror = reject;
        reader.readAsText(file);
      });
    }

    
    function processData(data) {
      return data; // Simply returning the data as it is
    }


// function readPropertiesFile() {
//   const fileInput = document.getElementById('fileInput');
//   const file = fileInput.files[0];
//   if (!file) {
//   console.error('No file selected.');
//   return;
//   }
//   if (!
//   file.name
//   .endsWith('.properties')) {
//   console.error('Please select a .properties file.');
//   return;
//   }
//   const reader = new FileReader();
//   reader.onload = function(event) {
//   const text = event.target.result;
//   console.log(text);

//   const properties = parseProperties(text);
//   console.log(properties);
//   x= JSON.stringify(properties);
//   console.log(x);
//   };
//   reader.onerror = function(event) {
//   console.error('Error reading the file:', event.target.error);
//   };
//   reader.readAsText(file);
//   }



//   function parseProperties(text) {
//   const properties = {};
//   const lines = text.split(/\r?\n/);
//   for (const line of lines) {
//   const trimmedLine = line.trim();
//   if (trimmedLine.length === 0 || trimmedLine.startsWith('#')) {
//   // Ignore empty lines or comments
//   continue;
//   }
//   const parts = trimmedLine.split('=');
//   const key = parts[0].trim();
//   const value = parts.slice(1).join('=').trim();
//   properties[key] = value;
//   }
  
//   return properties;
//   }


