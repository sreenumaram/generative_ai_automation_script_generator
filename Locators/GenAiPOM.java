package Qualitest.GenAI;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.PageFactory;

	public class GenAiPOM {

		WebDriver driver;
		
		public GenAiPOM(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
			
		}
		
		@FindBy(xpath = "//a[@href='https://openmrs.org/developers/']")
		WebElement developersMenu;
		

		 
		 @FindBy(xpath = "//a[text()='Product']")
		 WebElement productMenu;
		

		@FindBy(xpath="//a[text()='Integration']")
	 	WebElement integration;

		@FindBy(css="div.elementor-widget-container h2")
		WebElement heading;

		@FindBy(className="gt-current-lang")
		WebElement currentLangage;

		 @FindBy(css="a[data-gt-lang='nl']")
		 WebElement dutchLanguage;




		 @FindBy(xpath="@FindBy(xpath=(//div[@class='wrap']/a)[3]")
	    WebElement shop;

	    @FindBy(css=".orderby")
	    WebElement sort;

	    @FindBy(css="option[value='date']")
	    WebElement sortByLatest;

	    @FindBy(css=".woocommerce-loop-product__title")
	    WebElement bananaSweater;

	    @FindBy(xpath="//input[@id='quantity_65f1baf4672fa']")
	    WebElement increaseQuantity;

	    // @FindBy(css="button[value='73']")
	    // WebElement addToCart;

	    @FindBy(css="div[role='alert']")
	    WebElement cartMessage;

	    @FindBy(xpath="//a[@class='button wc-forward']")
	    WebElement viewCart;

		@FindBy(css=".woocommerce-shipping-destination strong")
		WebElement shippingAddress;
	}



	@FindBy(id ="drop-menu-value--region-dark-0")
    WebElement region;
	
	@FindBy(css="li[data-name='Washington']")
	WebElement washington; 

    @FindBy(css = "#autocomplete-search-2__input")
  WebElement searchInput;

 @FindBy(css = "div[id='autocomplete-search-2'] button[type='submit']")
  WebElement searchButton;

   @FindBy(css = ".item-count")
     WebElement resultCount;

    @FindBy(css = "a[class*='-register']")
     WebElement registerButton;

    @FindBy(xpath = "//a[@id='My Account']")
    WebElement createMyAccount;

    @FindBy(css = "#member-next1")
 WebElement continueButton;

    @FindBy(css="#validation-error-msg")
WebElement errorMessage;
    

	 @FindBy(css = "div[class='modal-close-title']")
     WebElement imageAd;
	 
	 @FindBy(css="div.cmp-text h1")
	 WebElement pageheading;




	 @FindBy(css = "h4.product-name")
WebElement products;

@FindBy(css = "a.increment")
WebElement quantityIncrement;

@FindBy(xpath = "//div[@class='product-action']/button")
WebElement addToCart;

@FindBy(css = "a.cart-icon")
WebElement cart;

@FindBy(xpath = "//button[text()='PROCEED TO CHECKOUT']")
WebElement checkout;

@FindBy(css = "input[class='promoCode']")
WebElement inputPromoCode;

@FindBy(xpath = "//button[text()='Apply']")
WebElement applyButton;

@FindBy(className = "promoInfo")
WebElement promoCodeStatus;

@FindBy(xpath = "//button[text()='Place Order']")
WebElement placeYourOrder;

@FindBy(css = "select[style='width: 200px;']")
WebElement chooseCountry;

@FindBy(css = "input[type='checkbox']")
WebElement termsCheckbox;

@FindBy(xpath = "//button[text()='Proceed']")
WebElement proccedOrder;
